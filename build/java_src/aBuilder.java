import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.CopyOption;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class aBuilder {

	private String application;
	private String version;
	private String outputDir;
	private boolean useVPN = false;
	private String gitUser;
	private String pw;
	private UsernamePasswordCredentialsProvider credentials;
	private static final int BUFFER_SIZE = 4096;

	public aBuilder(String application, String version, String outputDir, String user, String pw) {
		this.application = application;
		this.version = version;
		this.outputDir = outputDir;
		this.gitUser = user;
		this.pw = pw;
		this.credentials = new UsernamePasswordCredentialsProvider(user, pw);
	}

	public void read_application() {
		String ipAddress = "A41FIL001";
		String app = application + "-" + version;
		String inputFile = application + ".txt";
		String basePath = new File(aBuilder.class.getResource("aBuilder.class").getPath()).getAbsolutePath()
				.replace("aBuilder.class", "");
		String baseRepDir = basePath.replace("classes", "Repositories_Temp");

		InetAddress inet;
		try {
			inet = InetAddress.getByName(ipAddress);
			System.out.println("Sending Ping Request to " + ipAddress);
			useVPN = inet.isReachable(5000);
		} catch (UnknownHostException ukhost) {
			useVPN = false;
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (useVPN) {
				System.out.println("Using VPN");
			} else {
				System.out.println("Using direct connection");
			}
			configure_proxy(useVPN);
		}

		File f = new File(basePath + "..\\" + inputFile);
		if (!f.exists()) {
			// System.err.println("Application File not found. Try ../../" + inputFile);
			f = new File(basePath + "..\\..\\" + inputFile);
			if (!f.exists()) {
				System.err.println("Application File not found " + basePath + "../../" + inputFile);
				System.exit(1);
			}

		}

		File tDir = new File(outputDir + "/" + app);
		if (tDir.exists()) {
			try {
				delete(tDir);
			} catch (IOException e) {
				System.err.println("Could not delete target dir " + e.getMessage());
				System.exit(1);
			}
		}
		if (!tDir.mkdirs()) {
			System.err.println("Could not create target directory");
			System.exit(1);
		}

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(f));

			String line;
			boolean useLine = false;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("[")) {
					String currApp = line.substring(1, line.indexOf("]"));
					useLine = currApp.equalsIgnoreCase(app);
				} else if (line.length() > 0 && useLine && !line.startsWith("#")) {
					String[] items = line.split(";");
					System.out.println(Arrays.toString(items));
					String path = null;
					switch (items[0].toUpperCase()) {
					case "FILE":
						copy_Dir(items[1], items[2], app, outputDir + "/" + app + "/");
						break;
					case "GIT":
						path = download_repo(items[1], items[2], app, baseRepDir);
						outputDir = outputDir + "/" + app;
						if (items.length > 3 && !items[3].trim().equals("")) {
							path += "/" + items[3].trim();
						}
						copy_Dir(path, "", app, outputDir);
						break;
					case "ANT":
						path = download_repo(items[1], items[2], app, baseRepDir);
						// String opath = checkout_GIT(items[1], items[2] , app, baseRepDir);
						run_ANT(path + "/" + items[3]);
						outputDir = outputDir + "/" + app;
						if (items.length > 4 && !items[4].trim().equals("")) {
							path += "/" + items[4].trim();
						}
						if (items.length > 4 && !items[5].trim().equals("")) {
							outputDir += "/" + items[5].trim();
						}
						if (!(outputDir.endsWith("/") || outputDir.endsWith("\\"))) {
							outputDir += "/";
						}
						copy_Dir(path, "", app, outputDir);
					}
					System.out.println("SUCCESS");
				}
			}

		} catch (Exception e) {
			System.out.println("FAILED");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Deprecated
	private String checkout_GIT(String repo, String commit, String app, String gitBaseDir) throws Exception {
		File rep_dir = new File(gitBaseDir + repo);
		System.out.println("Using Repository in Directory " + gitBaseDir + repo);
		Git git = null;
		if (!rep_dir.exists()) {
			System.out.println("Repository " + repo + " not cloned yet");
			System.out.print("Cloning to " + gitBaseDir + repo + "..");
			git = Git.cloneRepository().setURI("https://bitbucket.org/avantumconsult/" + repo + ".git")
					.setCredentialsProvider(credentials).setCloneAllBranches(true).setDirectory(rep_dir).call();
			System.out.println("SUCCESS");

		} else {
			System.out.print("Pulling changes for repository " + repo + "...");
			git = Git.open(rep_dir);
			git.reset().setMode(ResetType.HARD).setRef("master").call();
			// git.checkout().setName("origin/master").setCreateBranch(true).call();
			// git.branchDelete().setBranchNames("Deployment").call();
			// git.pull().setCredentialsProvider(credentials).call();
			System.out.println("SUCCESS");
		}
		System.out.print("Checking out tag/commit " + commit + "...");

		git.checkout().setName("Deployment").setStartPoint(commit).setForce(true).call();

		// Git.open(rep_dir).
		System.out.println("SUCCESS");
		return gitBaseDir + repo;

	}

	private String download_repo(String repo, String commit, String app, String gitBaseDir) throws Exception {
		if (new File(gitBaseDir + commit + "/").exists()) {
			System.out.println("Deleting old files");
			deleteDir(gitBaseDir + commit + "/");
		}
		String url = "https://bitbucket.org/avantumconsult/" + repo + "/get/" + commit + ".zip";
		System.out.println("Downloading file from " + url);
		URL obj = new URL(url);
		HttpURLConnection httpConn = (HttpURLConnection) obj.openConnection();
		httpConn.setRequestMethod("GET");
		httpConn.setRequestProperty("Authorization",
				"Basic " + Base64.getEncoder().encodeToString(new String(gitUser + ":" + pw).getBytes()));

		int responseCode = httpConn.getResponseCode();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			String fileName = "";
			String disposition = httpConn.getHeaderField("Content-Disposition");
			String contentType = httpConn.getContentType();
			int contentLength = httpConn.getContentLength();

			fileName = gitBaseDir + commit + ".zip";

			System.out.println("Content-Type = " + contentType);
			System.out.println("Content-Disposition = " + disposition);
			System.out.println("Content-Length = " + contentLength);
			System.out.println("fileName = " + fileName);

			// opens input stream from the HTTP connection
			InputStream inputStream = httpConn.getInputStream();
			String saveFilePath = fileName;

			// opens an output stream to save into file
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);

			int bytesRead = -1;
			byte[] buffer = new byte[BUFFER_SIZE];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();
			httpConn.disconnect();
			System.out.println("File downloaded");

			if (new File(gitBaseDir + commit + "/").exists()) {
				System.out.println("Delete previous files");
				deleteDir(gitBaseDir + commit + "/");
			}
			System.out.println("Unzipping file");
			// Open the file
			try (ZipFile file = new ZipFile(fileName)) {
				FileSystem fileSystem = FileSystems.getDefault();
				// Get file entries
				Enumeration<? extends ZipEntry> entries = file.entries();

				// We will unzip files in this folder
				String uncompressedDirectory = gitBaseDir + "/" + commit + "/";
				Files.createDirectory(fileSystem.getPath(uncompressedDirectory));

				String zipBaseDir = null;

				// Iterate over entries
				while (entries.hasMoreElements()) {
					ZipEntry entry = entries.nextElement();
					// If directory then create a new directory in uncompressed folder
					if (entry.isDirectory()) {
						if (zipBaseDir == null) {
							zipBaseDir = uncompressedDirectory + entry.getName();
						}
						// System.out.println("Creating Directory:" + uncompressedDirectory +
						// entry.getName());
						Files.createDirectories(fileSystem.getPath(uncompressedDirectory + entry.getName()));
					}
					// Else create the file
					else {
						InputStream is = file.getInputStream(entry);
						BufferedInputStream bis = new BufferedInputStream(is);
						String uncompressedFileName = uncompressedDirectory + entry.getName();
						Path uncompressedFilePath = fileSystem.getPath(uncompressedFileName);
						Files.createFile(uncompressedFilePath);
						FileOutputStream fileOutput = new FileOutputStream(uncompressedFileName);
						while (bis.available() > 0) {
							fileOutput.write(bis.read());
						}
						fileOutput.close();
						// System.out.println("Written :" + entry.getName());
					}

				}
				return zipBaseDir;
			} catch (IOException e) {
				throw e;
			} finally {
				new File(fileName).deleteOnExit();
				
			}

		} else {
			throw new Exception("No file to download. Server replied HTTP code: " + responseCode);
		}

	}

	private void copy_Dir(String sourceDirS, String targetAdd, String target_app, String targetDir) throws Exception {
		System.out.print("Copy files from " + sourceDirS + " to " + targetDir + "...");
		File sourceDir = new File(sourceDirS);
		if (!sourceDir.exists()) {
			throw new Exception("Source Directory not found");
		}
		if (!targetAdd.trim().equals("")) {
			targetDir += "/" + targetAdd.trim();
		}

		File target = new File(targetDir);
		if (!target.exists() && !target.mkdirs()) {
			throw new Exception("Could not create target directory");
		}

		copyFileOrFolder(sourceDir, target, StandardCopyOption.REPLACE_EXISTING);
		System.out.println("SUCCESS");
	}

	public void run_ANT(String buildFileS) {
		System.out.println("Try to run ANT build file " + buildFileS);
		File buildFile = new File(buildFileS);
		Project p = new Project();
		p.setUserProperty("ant.file", buildFile.getAbsolutePath());
		p.init();
		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, buildFile);
		p.executeTarget(p.getDefaultTarget());
		
	}

	/**
	 * 1- Application 2- Version 3- Outputpath 4- GIT User 5- GIT Password
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new aBuilder(args[0], args[1], args[2], args[3], args[4]).read_application();
	}

	private void configure_proxy(boolean useVPN) {
		ProxySelector.setDefault(new ProxySelector() {
			final ProxySelector delegate = ProxySelector.getDefault();

			@Override
			public List<Proxy> select(URI uri) {
				// Filter the URIs to be proxied
				if (useVPN) {
					return Arrays
							.asList(new Proxy(Type.HTTP, InetSocketAddress.createUnresolved("91.229.169.122", 80)));
				}

				// revert to the default behaviour
				return delegate == null ? Arrays.asList(Proxy.NO_PROXY) : delegate.select(uri);
			}

			@Override
			public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
				if (uri == null || sa == null || ioe == null) {
					throw new IllegalArgumentException("Arguments can't be null.");
				}
			}
		});
	}

	private void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	public static void copyFileOrFolder(File source, File dest, CopyOption... options) throws IOException {
		if (source.isDirectory())
			copyFolder(source, dest, options);
		else {
			ensureParentFolder(dest);
			copyFile(source, dest, options);
		}
	}

	private static void copyFolder(File source, File dest, CopyOption... options) throws IOException {
		if (!dest.exists())
			dest.mkdirs();
		File[] contents = source.listFiles();
		if (contents != null) {
			for (File f : contents) {
				if (!f.isHidden() && !f.getName().startsWith(".")) {
					File newFile = new File(dest.getAbsolutePath() + File.separator + f.getName());

					if (f.isDirectory())
						copyFolder(f, newFile, options);
					else
						copyFile(f, newFile, options);
				}
			}
		}
	}

	private static void copyFile(File source, File dest, CopyOption... options) throws IOException {
		Files.copy(source.toPath(), dest.toPath(), options);
	}

	private static void ensureParentFolder(File file) {
		File parent = file.getParentFile();
		if (parent != null && !parent.exists())
			parent.mkdirs();
	}

	private void deleteDir(String path) {
		Path dir = Paths.get(path);
		try {
			Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					// System.out.println("Deleting file: " + file);
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					// System.out.println("Deleting dir: " + dir);
					if (exc == null) {
						Files.delete(dir);
						return FileVisitResult.CONTINUE;
					} else {
						throw exc;
					}
				}

			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
